
// load deps

var Code = require('code');   // assertion library
var Lab = require('lab');
var lab = exports.lab = Lab.script();

// variable setup

var expect = Code.expect;
var server = require('../index.js');


lab.experiment('sample', function () {

    lab.test('true is true', function (done) {

        expect(true).to.be.a.boolean().and.to.not.equal(false);
        done();
    });
});


lab.experiment('API', function () {

    lab.test('it serves the json file provided ', function (done) {

        server.inject('/api/local/fb', function (response) {

            var json = JSON.parse(response.payload);
            expect(json).to.be.an.array();
            expect(json[0]).to.contain('name');
            done();
        });
    });
});
